#!/bin/sh

c++ -o fibo.out fibo.cpp

rm -f fibo.csv

for i in 'seq 42' 
do
    ./fibo.out $i | tail -n 1 >> fibo.csv
done

gnuplot -e
