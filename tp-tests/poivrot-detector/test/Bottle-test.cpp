#include "../src/Bottle.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE( "init Chimay" ) {
    const Bottle b {"Chimay", 0.75, 0.08};
    REQUIRE(b._name == "Chimay");
    REQUIRE(b._vol == 0.75);
    REQUIRE(b._deg == 0.08);
}

TEST_CASE( "init Orange Pur Jus" ) {
    const Bottle b {"Orange Pur Jus", 1, 0};
    REQUIRE(b._name == "Orange Pur Jus");
    REQUIRE(b._vol == 1);
    REQUIRE(b._deg == 0);
}

TEST_CASE( "alcoholVolume Chimay" ) {
    const Bottle b {"Chimay", 0.75, 0.08};
    double volume;
    volume = alcoholVolume(b);
    REQUIRE(volume == 0.75);
}

TEST_CASE( "alcoholVolume Orange Pur Jus" ) {
    const Bottle b {"Orange Pur Jus", 1, 0};
    double volume;
    volume = alcoholVolume(b);
    REQUIRE(volume == 1);
}

TEST_CASE( "alcoholVolume Chimay + Orange" ) {
    const std::vector<Bottle> bs {{"Chimay", 0.75, 0.08},{"Orange Pur Jus", 1, 0}};
    double volume;
    volume = alcoholVolume(bs);
    REQUIRE(volume == 1.75);
}

TEST_CASE( "alcoholVolume Chimay + Chimay" ) {
    const std::vector<Bottle> bs {{"Chimay", 0.75, 0.08},{"Chimay", 0.75, 0.08}};
    double volume;
    volume = alcoholVolume(bs);
    REQUIRE(volume == 1.5);
}

TEST_CASE( "isPoivrot Chimay + Orange" ) {
    const std::vector<Bottle> bs {{"Chimay", 0.75, 0.08},{"Orange Pur Jus", 1, 0}};
    REQUIRE(isPoivrot(bs) == false);
}

TEST_CASE( "isPoivrot Chimay + Chimay" ) {
    const std::vector<Bottle> bs {{"Chimay", 0.75, 0.08},{"Chimay", 0.75, 0.08}};
    REQUIRE(isPoivrot(bs) == true);
}

TEST_CASE( "readBottles Chimay" ) {
    // TODO
}

TEST_CASE( "readBottles Chimay + Orange" ) {
    // TODO
}

