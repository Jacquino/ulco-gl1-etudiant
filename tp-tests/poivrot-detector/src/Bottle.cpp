#include "Bottle.hpp"

#include <sstream>

double alcoholVolume(const Bottle & b) {
    return b._vol;
}

double alcoholVolume(const std::vector<Bottle> & bs) {
    double volume = 0;
    for (int i = 0 ;i<bs.size(); i++){
        volume = volume + bs[i]._vol;
    }
    return volume;
}

bool isPoivrot(const std::vector<Bottle> & bs) {
    
    double vol_deg = 0;
    for (int i = 0 ;i<bs.size(); i++){
        if(bs[i]._deg != 0){
            vol_deg = vol_deg + bs[i]._vol*bs[i]._deg;
        }
    }
    if(vol_deg>0.1){
        return true;
    }else{
        return false;
    }
}

std::vector<Bottle> readBottles(std::istream & is) {
    // TODO
    return {};
}

