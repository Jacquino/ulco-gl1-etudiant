
#include <catch2/catch.hpp>
#include <mymaths/mul.hpp>

TEST_CASE( "mul 2", "[mul]" ) {
    REQUIRE( mul2(2) == 4 );
    REQUIRE( mul2(4) == 8 );
    REQUIRE( mul2(10) == 20);
    
    // REQUIRE( false );  // 
}

TEST_CASE( "mul n", "[mul]" ) {
    REQUIRE( muln(5,5) == 25 );
}
