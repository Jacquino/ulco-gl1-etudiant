
#include <catch2/catch.hpp>
#include <mymaths/add.hpp>

TEST_CASE( "add 2", "[add]" ) {
    REQUIRE( add2(2) == 4 );
    REQUIRE( add2(4) == 6 );
    REQUIRE( add2(10) == 12);
    
    // REQUIRE( false );  // 
}

TEST_CASE( "add n", "[add]" ) {
    REQUIRE( addn(5,5) == 10 );
}