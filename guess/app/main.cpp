#include <guess/guess.hpp>

#include <iostream>

int main() {
    std::cout << "this is guess" << std::endl;
    std::cout << "choice number limit of play" << std::endl;
    int nb_coup_max;
    std::cin >> nb_coup_max;
    Guess Guess1{generateRandom(),0,{},nb_coup_max};
    while (isLose(Guess1) == false){
        Guess1.current_number = playShoot(Guess1);
    }
    std::cout << Guess1.number_to_find << std::endl;

    return 0;
}

