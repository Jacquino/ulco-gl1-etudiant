#include <guess/guess.hpp>
#include <stdlib.h>
#include <time.h>
#include <iostream>
bool isEquals(int number_to_find,int current_number){
    if (number_to_find == current_number){
        return true;
    }else{
        return false;
    }
}

int generateRandom(){
    srand(time(NULL)); // Initialisation de la donnée seed
    int number = rand() % (100 + 1);
    return number;
}

std::vector<int> addValueInPrecNumber(int current_number,std::vector<int> prec_number){
    prec_number.push_back(current_number);
    return prec_number;
}

bool isLose(const Guess & g){
    if (g.prec_number.size()>=g.nb_coup_max){
        return true;
    }else{
        return false;
    }
}

int playShoot(const Guess & g){
    int num_choisi = 0;
    std::cout << "numéro choisi jusque là: " << std::endl;
    for (int i = 0 ;i<g.prec_number.size(); i++){
        std::cout << g.prec_number[i] << std::endl;
    }
    while (num_choisi > 100 || num_choisi < 1){
        std::cout << "nombre?" << std::endl;
        std::cin >> num_choisi;
        if (num_choisi > 100 || num_choisi < 1){
            std::cout << "invalid number " << std::endl;
        }
    }
    return num_choisi;  
}


