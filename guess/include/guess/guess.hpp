#pragma once
#include <vector>
/// Structure du jeu : Gessins Game.
/// Exemple d'initialisation :
/// const Guess Guess1{52,0,{}};
/// std::cout << "numéro à trouver: " << Guess1.number_to_find << std::endl;
/// std::cout << "numéro actuellement sélectionner: " << Guess1.current_number << std::endl;
/// std::cout << "numéro choisi jusque là: " << Guess1.prec_number << std::endl;
/// std::cout << "nombre de coup max choisi: " << Guess1.nb_coup_max << std::endl;
struct Guess {
    int number_to_find;
    int current_number;
    std::vector<int> prec_number;
    int nb_coup_max;
};

/// Test l'égalité entre deux nombres
bool isEquals(int number_to_find,int current_number);
/// Génére un nombre aléatoire entre 1 et 100 
int generateRandom();
/// retourne prec_number avec la valeur ajouté
std::vector<int> addValueInPrecNumber(int current_number,std::vector<int> prec_number);
/// verifie si le joueur a atteint ça limite de coup 
bool isLose(const Guess & g);
/// joue un coup et retourne la valeur choisi
int playShoot(const Guess & g);


