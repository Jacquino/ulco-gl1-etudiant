cmake_minimum_required( VERSION 3.0 )
project( guess )
include_directories( include )

# library
add_library( guess-lib
    src/guess/guess.cpp )

# executable
add_executable( guess-app
  app/main.cpp )
target_link_libraries( guess-app guess-lib )

# install
install( TARGETS guess-app DESTINATION bin )

# testing
add_executable( guess-test
    test/guess/guess-test.cpp
    test/main.cpp )
target_link_libraries( guess-test guess-lib )
enable_testing()
add_test( NAME guess-test COMMAND guess-test )

