#include <guess/guess.hpp>

#include <catch2/catch.hpp>

TEST_CASE( "init struct guess " ) {
    const Guess Guess1{52,0,{},4};
    REQUIRE( Guess1.number_to_find == 52 );
    REQUIRE( Guess1.current_number == 0 );
    REQUIRE( Guess1.prec_number.size() == 0 );
    REQUIRE( Guess1.nb_coup_max == 4 );
}

TEST_CASE( "test equals in true case" ) {
    REQUIRE( isEquals(52,52) == true );
}

TEST_CASE( "test equals in false case" ) {
    REQUIRE( isEquals(22,52) == false );
}

TEST_CASE( "random number generate is between 1 and 100" ) {
    int myNumber = generateRandom();
    REQUIRE( myNumber >= 1 );
    REQUIRE( myNumber <= 100 );
}

TEST_CASE( "number is add to prec_number" ) {
    std::vector<int> prec_number = {1,2};
    prec_number = addValueInPrecNumber(3,prec_number);
    REQUIRE( prec_number.size() == 3 );
    REQUIRE( prec_number[2] == 3 );  
}

TEST_CASE( "the player lose the game" ) {
    const Guess Guess1{52,0,{1,2},1};
    REQUIRE(isLose(Guess1) == true);
}

TEST_CASE( "the player don't lose the game" ) {
    const Guess Guess1{52,0,{},5};
    REQUIRE(isLose(Guess1) == false);
}



