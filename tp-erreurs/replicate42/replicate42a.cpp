#include <iostream>
#include <exception>
#include <vector>

enum status_t { OK, TOO_LOW, TOO_HIGH, IS_37, IS_NEGATIVE,IS_NULL };

status_t STATUS;

// Reads a number in stdin and sets STATUS (to OK, IS_37 or IS_NEGATIVE).
int readPositiveButNot37() {
    std::string line;
    std::cout << "enter num: ";
    std::flush(std::cout);
    std::getline(std::cin, line);
    int num = stod(line);
    if (num == 37) {
        STATUS = IS_37;
        return -1;
    }
    if (num < 0) {
        STATUS = IS_NEGATIVE;
        return -1;
    }
    if (num == 0){
        STATUS = IS_NULL;
        return -1;
    }
    STATUS = OK;
    return num;
}

// Replicates, n times, the integer 42 in a vector and sets STATUS. If n is
// invalid (< 0 or > 42), returns an empty vector.
std::vector<int> replicate42(int n) {
    if (n < 1) {
        STATUS = TOO_LOW;
        return {};
    }
    if (n > 42) {
        STATUS = TOO_HIGH;
        return {};
    }
    STATUS = OK;
    return std::vector<int>(n, 42);
}

int main() {

    // read number
    const int num = readPositiveButNot37();
    if (STATUS == status_t::IS_37){
        std::cerr << "error: 37\n";
        exit(-1);
    }else if(STATUS == status_t::IS_NEGATIVE){
        std::cerr << "error: negatif\n";
        exit(-1);
    }else if(STATUS !=OK){
        std::cout << "unexpected error"<<std::endl;
        exit(-1);
    }

    std::cout << "num = " << num << std::endl;

    // build vector
    const auto v = replicate42(num);
    if (STATUS != status_t::OK){
        switch (STATUS)
        {
            case status_t::TOO_LOW:
                std::cerr <<"error : to low\n";
                break;

            case status_t::TOO_HIGH:
                std::cerr <<"error : to high\n";
                break;
        
            default:
            std::cout <<"unexpected error"<<std::endl;
                break;
        }
        exit(-1);
    }
    // TODO handle errors

    // print vector
    for (const int x : v)
        std::cout << x << " ";
    std::cout << std::endl;

    return 0;
}

