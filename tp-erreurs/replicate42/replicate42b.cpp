#include <iostream>
#include <exception>
#include <vector>

enum class NumError { TooLow, TooHigh, Is37, IsNegative };

// Reads a number in stdin. Throws a NumError if 37 or negative.
int readPositiveButNot37() {
    std::string line;
    std::cout << "enter num: ";
    std::flush(std::cout);
    std::getline(std::cin, line);
    int num = stod(line);
    if (num == 37)
        throw NumError::Is37;
    if (num<0)
        throw NumError::IsNegative;
    return num;
}

// Reads a number in stdin. Throws a NumError if too low or too high.
std::vector<int> replicate42(int n) {
    if( n<1)
        throw NumError::TooLow;
    if(n>42)
        throw NumError::TooHigh;
    return std::vector<int>(n, 42);
}

int main() {

    try{
        // read number
        const int num = readPositiveButNot37();
        std::cout << "num = " << num << std::endl;

        // build vector
        const auto v = replicate42(num);

        // print vector
        for (const int x : v)
            std::cout << x << " ";
        std::cout << std::endl;
    }
    catch (const NumError & e){
        std::cerr <<"error\n";
        switch (e)
        {
            case NumError::TooLow:
                std::cerr<<"error too low";
                break;
            case NumError::TooHigh:
                std::cerr<<"error too high";
                break;
            default:
                std::cerr<<"unexpected error";
                break;
        }
    }
    return 0;
}

